---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mailserver
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mailserver
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: mailserver
      annotations:
        #container.apparmor.security.beta.kubernetes.io/mailserver: runtime/default
        linstor.csi.linbit.com/on-storage-lost: remove
    spec:
      hostname: mail
      nodeSelector:
        kubernetes.io/arch: amd64
      containers:
        - name: mailserver
          image: docker.io/mailserver/docker-mailserver:12.0.0
          imagePullPolicy: Always
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: false
            runAsUser: 0
            runAsGroup: 0
            runAsNonRoot: false
            privileged: false
            capabilities:
              add:
                # file permission capabilities
                - CHOWN
                - FOWNER
                - MKNOD
                - SETGID
                - SETUID
                - DAC_OVERRIDE
                # network capabilities
                - NET_ADMIN  # needed for F2B
                - NET_RAW    # needed for F2B
                - NET_BIND_SERVICE
                # miscellaneous  capabilities
                - SYS_CHROOT
                - SYS_PTRACE
                - KILL
              drop: [ALL]
            seccompProfile:
              type: RuntimeDefault
          # You want to tune this to your needs. If you disable ClamAV,
          #   you can use less RAM and CPU. This becomes important in
          #   case you're low on resources and Kubernetes refuses to
          #   schedule new pods.
          resources:
            limits:
              memory: 4Gi
              cpu: "6"
            requests:
              memory: 2Gi
              cpu: 600m
          volumeMounts:
            # Config
            - name: files
              subPath: postfix-main.cf
              mountPath: /tmp/docker-mailserver/postfix-main.cf
              readOnly: true
            - name: files
              subPath: postfix-accounts.cf
              mountPath: /tmp/docker-mailserver/postfix-accounts.cf
              readOnly: true
            - name: files
              subPath: postfix-virtual.cf
              mountPath: /tmp/docker-mailserver/postfix-virtual.cf
              readOnly: true
            - name: cert
              subPath: tls.crt
              mountPath: /cert/tls.crt
              readOnly: true
            - name: cert
              subPath: tls.key
              mountPath: /cert/tls.key
              readOnly: true
            - name: dkim-conf
              subPath: KeyTable
              mountPath: /tmp/docker-mailserver/opendkim/KeyTable
              readOnly: true
            - name: dkim-conf
              subPath: SigningTable
              mountPath: /tmp/docker-mailserver/opendkim/SigningTable
              readOnly: true
            - name: dkim-conf
              subPath: TrustedHosts
              mountPath: /tmp/docker-mailserver/opendkim/TrustedHosts
              readOnly: true
            - name: dkim-secret
              subPath: mail.private
              mountPath: /tmp/docker-mailserver/opendkim/keys/mikesweb.site/mail.private
              readOnly: true
            - name: dkim-secret
              subPath: mail.txt
              mountPath: /tmp/docker-mailserver/opendkim/keys/mikesweb.site/mail.txt
              readOnly: true
            # PVCs
            - name: data
              mountPath: /var/mail
              subPath: data
              readOnly: false
            - name: data
              mountPath: /var/mail-state
              subPath: state
              readOnly: false
            - name: data
              mountPath: /var/log/mail
              subPath: log
              readOnly: false
            # other
            - name: tmp-files
              mountPath: /tmp
              readOnly: false
          ports:
            - name: transfer
              containerPort: 25
              protocol: TCP
            - name: esmtp-implicit
              containerPort: 465
              protocol: TCP
            - name: esmtp-explicit
              containerPort: 587
            - name: imap-implicit
              containerPort: 993
              protocol: TCP
            - name: sieve
              containerPort: 4190
              protocol: TCP
          envFrom:
            - configMapRef:
                name: env
      restartPolicy: Always
      volumes:
        # configuration files
        - name: files
          configMap:
            name: files
        # PVCs
        - name: data
          persistentVolumeClaim:
            claimName: data
        # other
        - name: tmp-files
          emptyDir: {}
        - name: cert
          secret:
            secretName: tls
        - name: dkim-secret
          secret:
            secretName: dkim
        - name: dkim-conf
          configMap:
            name: dkim

