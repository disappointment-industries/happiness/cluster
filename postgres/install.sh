#!/bin/bash

helm repo add postgres-operator https://opensource.zalando.com/postgres-operator/charts/postgres-operator/
helm repo update
kubectl create ns postgres || true
kubectl config set-context --current --namespace postgres
helm install zalando-operator postgres-operator/postgres-operator -f values.yml
kubectl apply -f db.yml
