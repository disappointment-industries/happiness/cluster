#!/bin/sh

helm repo add ot-helm https://ot-container-kit.github.io/helm-charts/

kubectl create ns redis || true

kubectl create secret generic redis-secret \
    --from-literal=password=${PASSWORD:-password} -n redis

helm upgrade redis-cluster ot-helm/redis-cluster \
  --set redisCluster.clusterSize=3 --set priorityClassName=apps --install --namespace redis

